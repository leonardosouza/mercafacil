    require("dotenv").config();

    const express = require("express");
    const app = express();

    app.use(express.static('public'));

    const vendas = require("./app/routes/vendas");
    const clientes = require("./app/routes/clientes");
    const geo = require("./app/routes/geo");

    // inicia as rotas
    vendas(app);
    geo(app);
    clientes(app);

    // rotas estaticas
    const public = require("./app/routes/public");
    public(app);

    app.listen(3000);