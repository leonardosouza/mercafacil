const vendas = require('../controllers/vendas');
module.exports = app => {
    app.get("/vendas/faturamento", async (req, res) => {
        const {
            inicio,
            fim
        } = req.query;
        vendas.faturamentoTotal(inicio, fim)
            .then(result => {

                if (result.rows[0].total) {
                    res.status(200).json(result.rows[0]);
                    return;
                }

                res.status(404).json({
                    "total": 0
                });
            })
            .catch(error => {
                res.status(500).json(error);
            })
    });

    app.get("/vendas/count", async (req, res) => {
        const {
            inicio,
            fim
        } = req.query;
        vendas.cupons(inicio, fim)
            .then(result => {

                if (result.rows[0].total) {
                    res.status(200).json(result.rows[0]);
                    return;
                }

                res.status(404).json({
                    "total": 0
                });
            })
            .catch(error => {
                res.status(500).json(error);
            })
    });

    app.get("/vendas/genero", async (req, res) => {
        const {
            inicio,
            fim
        } = req.query;
        vendas.qtdComprasGenero(inicio, fim)
            .then(result => {

                if (result.rows) {
                    res.status(200).json(result.rows);
                    return;
                }

                res.status(404).json({
                    "total": 0
                });
            })
            .catch(error => {
                res.status(500).json(error);
            })
    });

    app.get("/vendas/genero/faturamento", async (req, res) => {
        const {
            inicio,
            fim
        } = req.query;
        vendas.faturamentoGenero(inicio, fim)
            .then(result => {

                if (result.rows) {
                    res.status(200).json(result.rows);
                    return;
                }

                res.status(404).json({
                    "total": 0
                });
            })
            .catch(error => {
                res.status(500).json(error);
            })
    });

    app.get("/vendas/cidades", async (req, res) => {
        const {
            inicio,
            fim
        } = req.query;
        vendas.qtdComprasCidade(inicio, fim)
            .then(result => {

                if (result.rows) {
                    res.status(200).json(result.rows);
                    return;
                }

                res.status(404).json({
                    "total": 0
                });
            })
            .catch(error => {
                res.status(500).json(error);
            })
    });

    app.get("/vendas/bairros/faturamento", async (req, res) => {
        const {
            inicio,
            fim
        } = req.query;
        vendas.faturamentoBairro(inicio, fim)
            .then(result => {

                if (result.rows) {
                    res.status(200).json(result.rows);
                    return;
                }

                res.status(404).json({
                    "total": 0
                });
            })
            .catch(error => {
                res.status(500).json(error);
            })
    });

    app.get("/vendas/identificacao", async (req, res) => {
        const {
            inicio,
            fim
        } = req.query;
        try {

            let {
                rows: faturamentoCliente
            } = await vendas.faturamentoCliente(inicio, fim);
            const {
                rows: geral
            } = await vendas.faturamentoTotal(inicio, fim);
            console.log(geral);
            faturamentoCliente = faturamentoCliente.map(cliente => {
                cliente.porcentagem = parseFloat(cliente.total) / parseFloat(geral[0].total);
                return cliente;
            });

            res.status(200).json(faturamentoCliente);
        } catch (error) {
            res.status(500).json(error);
        }
    });

}