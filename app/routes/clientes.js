const clientes = require('../controllers/clientes');

module.exports = app => {
    app.get("/clientes/count", async (req, res) => {
        clientes.count()
            .then(result => {

                if (result.rows) {
                    res.status(200).json(result.rows[0]);
                    return;
                }

                res.status(404).json({
                    "total": 0
                });
            })
            .catch(error => {
                res.status(500).json(error);
            })
    });
}