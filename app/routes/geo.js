const geo = require('../controllers/geo');

module.exports = app => {
    app.get("/geo/cidades/count", async (req, res) => {
        geo.count("cidades")
            .then(result => {

                if (result.rows) {
                    res.status(200).json(result.rows);
                    return;
                }

                res.status(404).json({
                    "total": 0
                });
            })
            .catch(error => {
                res.status(500).json(error);
            })
    });

    app.get("/geo/estados/count", async (req, res) => {
        geo.count("estados")
            .then(result => {

                if (result.rows) {
                    res.status(200).json(result.rows);
                    return;
                }

                res.status(404).json({
                    "total": 0
                });
            })
            .catch(error => {
                res.status(500).json(error);
            })
    });

    app.get("/geo/bairros/count", async (req, res) => {
        geo.count("bairros")
            .then(result => {

                if (result.rows) {
                    res.status(200).json(result.rows);
                    return;
                }

                res.status(404).json({
                    "total": 0
                });
            })
            .catch(error => {
                res.status(500).json(error);

            })
    });
}