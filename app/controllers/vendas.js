const db = require("../utils/db");

class Vendas {

    constructor() {
        this.$db = db.client();
    }
    
    /**
     * @description Responsavel retornar o faturamento total em um determinado periodo
     * @param {Date} inicio 
     * @param {Date} fim 
     * @returns {Object}
     */
    async faturamentoTotal(inicio, fim) {
        const query = "select SUM(faturamento) as total from public.vendas where data between $1 and $2;";
        return await this.$db.query(query, [inicio, fim]);
    }
    /**
     * @description Responsavel retornar de compras realizadas em um determinado periodo
     * @param {Date} inicio 
     * @param {Date} fim 
     * @returns {Object}
     */
    async cupons(inicio, fim) {
        const query = "select COUNT (id_venda) as total from public.vendas where data between $1 and $2;";
        return await this.$db.query(query, [inicio, fim]);
    }

    /**
     * @description Responsavel retornar qtd de compras segmentada por genero de um determinado periodo
     * @param {Date} inicio 
     * @param {Date} fim 
     * @returns {Object}
     */
    async qtdComprasGenero(inicio, fim) {

        const query = `select 
                            CASE 
                                genero when '' then 'I'
                                else genero 
                            END as genero,
                            COUNT(v.id_cliente) total
                        from public.vendas v
                        inner join 	public.clientes c on v.id_cliente = c.id_cliente 
                        where data between $1 and $2
                        group by genero;`;


        return await this.$db.query(query, [inicio, fim]);
    }

    /**
     * @description Responsavel retornar o faturamento total por genero um determinado periodo
     * @param {Date} inicio 
     * @param {Date} fim 
     * @returns {Object}
     */
    async faturamentoGenero(inicio, fim) {
        const query = `select 
                            CASE 
                                genero when '' then 'I'
                                else genero 
                            END as genero,
                            SUM(v.faturamento) total
                        from public.vendas v
                        inner join 	public.clientes c on v.id_cliente = c.id_cliente 
                        where data between $1 and $2
                        group by genero;`;


        return await this.$db.query(query, [inicio, fim]);
    }

    /**
     * @description Responsavel retornar o faturamento total de compras realizadas na cidades em um determinado periodo
     * @param {Date} inicio 
     * @param {Date} fim 
     * @returns {Object}
     */
    async qtdComprasCidade(inicio, fim) {

        const query = `select 
                            ci.cidade,
                            COUNT(v.id_venda) total
                        from public.vendas v
                            inner join public.clientes c on v.id_cliente = c.id_cliente 
                            inner join public.bairros b on b.id_bairro = c.id_bairro
                            inner join public.cidades ci on ci.id_cidade = b.id_cidade
                        where data between $1 and $2
                        group by cidade
                        order by total DESC; `;

        return await this.$db.query(query, [inicio, fim]);

    }

    /**
     * @description Responsavel retornar o faturamento total dos bairros de um determinado periodo
     * @param {Date} inicio 
     * @param {Date} fim 
     * @returns {Object}
     */
    async faturamentoBairro(inicio, fim) {
        const query = `select 
                            b.bairro,
                            COUNT(v.faturamento) total
                        from public.vendas v
                            inner join public.clientes c on v.id_cliente = c.id_cliente 
                            inner join public.bairros b on b.id_bairro = c.id_bairro
                            where data between $1 and $2
                        group by b.id_bairro
                        order by total DESC; `;

        return await this.$db.query(query, [inicio, fim]);
    }

    /**
     * @description Responsavel retornar o faturamento total dos clientes de um determinado periodo
     * @param {Date} inicio 
     * @param {Date} fim 
     * @returns {Object}
     */
    async faturamentoCliente(inicio, fim) {
        const query = `select
                            v.id_cliente,
                            SUM(v.faturamento) total
                        from public.vendas v
                        where data between $1 and $2
                            group by v.id_cliente
                        order by v.id_cliente;`;

        return await this.$db.query(query, [inicio, fim]);
    }
}

module.exports = new Vendas();