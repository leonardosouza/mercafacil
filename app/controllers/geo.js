const db = require("../utils/db");

class Geo {

    constructor() {
        this.$db = db.client();
    }

    /**
     * @description Responsavel por retornar o total de registros do tipo enviado
     * @param {String} tipo 
     */
    async count(tipo) {

        if(['cidades', 'estados', 'bairros'].indexOf(tipo) < 0) {
            throw new Error('tipo invalido');
        }

        const query = `select count(*) total from ${tipo}`;

        return await this.$db.query(query);
    }

}

module.exports = new Geo();