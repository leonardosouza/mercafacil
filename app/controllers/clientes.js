const db = require("../utils/db");

class Clientes {

    constructor() {
        this.$db = db.client();
    }
    
    /**
     * @description Responsavel retornar a quantidade total de clientes
     * @returns {Object}
     */
    async count() {
        const query = `select count(*) total from clientes`;

        return await this.$db.query(query);
    }

}

module.exports = new Clientes();