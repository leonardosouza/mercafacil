const { Client } = require('pg')
const client = new Client()

class Db {

    client() {
        const client = new Client({
            user: process.env.PGUSER,
            host: process.env.PGHOST,
            database: process.env.PGDATABASE,
            password: process.env.PGPASSWORD,
            port: process.env.PGPORT,
        })

        client.connect();

        return client;
    }
}

module.exports = new Db();