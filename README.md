# Mercateste

Desenvolver uma dashboard WEB com sua tecnologia de preferêrcia que realize acesso a seguinte base de dados:

* servidor: ******
* porta: ******
* usuário: ******
* senha: ******

E gere os seguintes relatórios, com opção de filtro por data

* `vendas_info`:
    * Faturamento total (B)
    * Número de cupons de venda (B)
* `clientes_info`:
    * Número de clientes (B)
* `geo_info`: 
    * Número de estados (B)
    * Número de cidades (B)
    * Número de bairros (B)
* `vendas_cli`:
    * Gráfico com quantidade de compras por genero (B)
	* Gráfico com faturamento por genero (B)
* `geo_vendas`:
    * Gráfico com quantidade de compras por cidade (B)
	* Gráfico com faturamento por bairro (B)
* `porcentagem_identificacao`
    * Porcentagem de identificação de vendas do mercado: (faturamento de vendas com cliente/faturamento total) (B)

-----
# Instruções

* `Instalações dos modulos`
```
npm install
```

* `Criação de arquivo .env`
```
touch .env
```

* `Inclusão de variaveis no arquivo .env`
```
PGHOST=
PGUSER=
PGDATABASE=
PGPASSWORD=
PGPORT=
```

* `Rodando a aplicação`
```
npm run dev
```

### Front

```
    http://localhost:3000/
```
> **NOTA:** Na área de gráfico é possível desabilitar o dado ao clicar na legenda do mesmo.

### Back

Endpoints mapeados:

> **GET** - /vendas/faturamento/total

> **GET** /vendas/count`

> **GET** /vendas/genero

> **GET** /vendas/genero/faturamento

> **GET**/vendas/cidades

> **GET** /vendas/bairros/faturamento

> **GET** /vendas/identificacao

> **GET** /geo/cidades/count

> **GET** /geo/estados/count

> **GET** /geo/bairros/count

> **GET** /clientes/count
