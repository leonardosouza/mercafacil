var app = new Vue({
    el: '#app',
    data: {
        indicadores: {
            faturamentoTotal: 0,
            totalCupons: 0,
            clientes: 0,
            localidade: {
                cidades: 0,
                estados: 0,
                bairros: 0
            }
        },
        inicio: "2018-06-01",
        fim: "2018-07-31",
    },
    watch: {
        inicio: function () {
            this.refresh();
        },
        fim: function () {
            this.refresh();
        }
    },
    methods: {
        async localidades(tipo) {
            await fetch(`http://localhost:3000/geo/${tipo}/count`, {})
                .then(result => {
                    return result.json();
                })
                .then(data => {
                    this.indicadores.localidade[tipo] = data[0].total;
                });
        },
        async faturamentoTotal() {
            console.log(
                `http://localhost:3000/vendas/faturamento?inicio=${this.inicio}&fim=${this.fim}`
            );
            await fetch(
                    `http://localhost:3000/vendas/faturamento?inicio=${this.inicio}&fim=${this.fim}`, {}
                )
                .then(result => {
                    return result.json();
                })
                .then(data => {
                    this.indicadores.faturamentoTotal = data.total;
                });
        },
        async vendas() {
            console.log(`http://localhost:3000/vendas/count?inicio=${this.inicio}&fim=${this.fim}`);
            await fetch(`http://localhost:3000/vendas/count?inicio=${this.inicio}&fim=${this.fim}`, {})
                .then(result => {
                    return result.json();
                })
                .then(data => {
                    this.indicadores.totalCupons = data.total;
                });
        },
        async clientes() {
            console.log(`http://localhost:3000/clientes/count?inicio=${this.inicio}&fim=${this.fim}`);
            await fetch(
                    `http://localhost:3000/clientes/count?inicio=${this.inicio}&fim=${this.fim}`, {})
                .then(result => {
                    return result.json();
                })
                .then(data => {
                    this.indicadores.clientes = data.total;
                });
        },
        refresh() {
            this.faturamentoTotal();
            this.vendas();
            this.clientes();
        }
    },
    created() {
        this.localidades('cidades');
        this.localidades('estados');
        this.localidades('bairros');
        this.refresh();
    }
})