const colors = ['#007BFF', '#D36BE7', '#FF6BB3', '#FF917E', '#FFC65F', '#F9F871', '#9BDE7E', '#4BBC8E', '#039590',
    '#1C6E7D', '#2F4858'
];

Vue.component('pie-chart', {
    extends: VueChartJs.Pie,
    mixins: [VueChartJs.mixins.reactiveProp],
    props: ['options'],
    mounted() {
        this.renderChart(this.chartData, this.options)
    }
});

Vue.component('bar-chart', {
    extends: VueChartJs.Bar,
    mixins: [VueChartJs.mixins.reactiveProp],
    props: ['options'],
    mounted() {
        this.renderChart(this.chartData, this.options)
    }
});

var app = new Vue({
    el: '#app',
    data: {
        inicio: "2018-06-01",
        fim: "2018-07-31",
        dataGraph_VG: {},
        dataGraph_FG: {},
        dataGraph_VC: {},
        dataGraph_FB: {}
    },
    watch: {
        inicio: function () {
            this.refresh();
        },
        fim: function () {
            this.refresh();
        }
    },
    methods: {
        async refresh() {
            await this.comprasGenero();
            await this.comprasCidade();

            await this.faturamentoGenero();
            await this.faturamentoBairro();
        },
        async comprasGenero() {
            await fetch(
                    `http://localhost:3000/vendas/genero?inicio=${this.inicio}&fim=${this.fim}`, {})
                .then(result => {
                    return result.json();
                })
                .then(data => {

                    this.dataGraph_VG = {
                        labels: data.map(info => {
                            return info.genero;
                        }),
                        datasets: [{
                            label: "Data One",
                            backgroundColor: _.shuffle(colors),
                            data: data.map(info => {
                                return parseInt(info.total);
                            })
                        }]
                    };

                });
        },
        async comprasCidade() {
            await fetch(
                    `http://localhost:3000/vendas/cidades?inicio=${this.inicio}&fim=${this.fim}`, {}
                )
                .then(result => {
                    return result.json();
                })
                .then(data => {
                    //pegamos somente o top 10
                    const top10 = data.slice(0, 10);

                    this.dataGraph_VC = {
                        labels: top10.map(info => {
                            return info.cidade;
                        }),
                        datasets: [{
                            label: "Vendas por cidade",
                            backgroundColor: _.shuffle(colors),
                            data: top10.map(info => {
                                return parseInt(info.total);
                            })
                        }]
                    };

                });
        },
        async faturamentoGenero() {
            await fetch(
                    `http://localhost:3000/vendas/genero/faturamento?inicio=${this.inicio}&fim=${this.fim}`, {}
                )
                .then(result => {
                    return result.json();
                })
                .then(data => {

                    this.dataGraph_FG = {
                        labels: data.map(info => {
                            return info.genero;
                        }),
                        datasets: [{
                            label: "Data One",
                            backgroundColor: _.shuffle(colors),
                            data: data.map(info => {
                                return parseFloat(info.total);
                            })
                        }]
                    };

                });
        },
        async faturamentoBairro() {
            await fetch(
                    `http://localhost:3000/vendas/bairros/faturamento?inicio=${this.inicio}&fim=${this.fim}`, {}
                )
                .then(result => {
                    return result.json();
                })
                .then(data => {
                    //pegamos somente o top 10
                    const top10 = data.slice(0, 10);
                    this.dataGraph_FB = {
                        labels: top10.map(info => {
                            return info.bairro;
                        }),
                        datasets: [{
                            backgroundColor: _.shuffle(colors),
                            data: top10.map(info => {
                                return parseInt(info.total);
                            })
                        }]
                    };

                });
        }
    },
    mounted() {
        this.refresh();
    }
})